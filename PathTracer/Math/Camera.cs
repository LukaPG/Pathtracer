﻿namespace PathTracer.Math
{
    public class Camera
    {
        public OrthoNormalBasis Axis { get; private set; }
        private readonly double lensRadius;
        private readonly double u0, u1, v0, v1;
        private readonly double d;
        private readonly Vec3 centre, corner, across, up;

        public Camera(Vec3 eye, Vec3 dir, Vec3 up, double aperture,
                      double left, double right, double top, double bottom, double distance)
        {
            Axis = OrthoNormalBasis.FromZY(dir, up);
            lensRadius = aperture / 2;
            u0 = left;
            u1 = right;
            v0 = top;
            v1 = bottom;
            d = distance;
            centre = eye;
            corner = centre + Axis.Transform(new Vec3(u0, v0, d));
            across = Axis.X * (u1 - u0);
            this.up = Axis.Y * (v1 - v0);
        }

        public Ray Ray(double x, double y, double xi1, double xi2)
        {
            var origin = centre + Axis.Transform(new Vec3(2 * ((xi1 * xi1) - 0.5) * lensRadius,
                                                          2 * ((xi2 * xi2) - 0.5) * lensRadius, 
                                                          0));
            var target = corner + across * x + this.up * y;
            return Math.Ray.FromTwoPoints(origin, target);
        }
    }
}
