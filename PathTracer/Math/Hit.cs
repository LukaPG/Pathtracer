﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathTracer.Math
{
    public class Hit
    {
        public double Distance { get; set; }
        public Vec3 Position { get; set; }
        public Vec3 Normal { get; set; }
    }
}
