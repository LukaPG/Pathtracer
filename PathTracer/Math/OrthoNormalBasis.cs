﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathTracer.Math
{
    public class OrthoNormalBasis
    {
        public Vec3 X { get; private set; }
        public Vec3 Y { get; private set; }
        public Vec3 Z { get; private set; }

        public OrthoNormalBasis(Vec3 x, Vec3 y, Vec3 z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vec3 Transform(Vec3 position) => X * position.x + Y * position.y + Z * position.z;

        public static OrthoNormalBasis FromXY(Vec3 x, Vec3 y)
        {
            var xx = x.Normalised;
            var zz = x.Cross(y).Normalised;
            var yy = zz.Cross(xx);
            return new OrthoNormalBasis(xx, yy, zz);
        }

        public static OrthoNormalBasis FromYX(Vec3 y, Vec3 x)
        {
            var yy = y.Normalised;
            var zz = x.Cross(y).Normalised;
            var xx = yy.Cross(zz);
            return new OrthoNormalBasis(xx, yy, zz);
        }

        public static OrthoNormalBasis FromXZ(Vec3 x, Vec3 z)
        {
            var xx = x.Normalised;
            var yy = x.Cross(z).Normalised;
            var zz = xx.Cross(yy);
            return new OrthoNormalBasis(xx, yy, zz);
        }

        public static OrthoNormalBasis FromZX(Vec3 z, Vec3 x)
        {
            var zz = z.Normalised;
            var yy = z.Cross(x).Normalised;
            var xx = yy.Cross(zz);
            return new OrthoNormalBasis(xx, yy, zz);
        }

        public static OrthoNormalBasis FromYZ(Vec3 y, Vec3 z)
        {
            var yy = y.Normalised;
            var xx = y.Cross(z).Normalised;
            var zz = xx.Cross(yy);
            return new OrthoNormalBasis(xx, yy, zz);
        }

        public static OrthoNormalBasis FromZY(Vec3 z, Vec3 y)
        {
            var zz = z.Normalised;
            var xx = y.Cross(z).Normalised;
            var yy = zz.Cross(xx);
            return new OrthoNormalBasis(xx, yy, zz);
        }

        public static OrthoNormalBasis FromZ(Vec3 z)
        {
            const double coincident = 0.9999d;
            var zz = z.Normalised;
            var x = System.Math.Abs(zz.Dot(Vec3.XAxis)) > coincident ? Vec3.YAxis : Vec3.XAxis;
            var xx = x.Cross(zz).Normalised;
            var yy = zz.Cross(xx).Normalised;
            return new OrthoNormalBasis(xx, yy, zz);
        }
    }
}
