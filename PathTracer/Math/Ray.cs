﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathTracer.Math
{
    public class Ray
    {
        public Vec3 Origin { get; set; }
        public Vec3 Direction { get; set; }

        public Ray(Vec3 origin, Vec3 direction)
        {
            Origin = origin;
            Direction = direction.Normalised;
        }

        public Vec3 PositionAlong(double distance)
        {
            return Origin + (Direction * distance);
        }

        public static Ray FromTwoPoints(Vec3 a, Vec3 b)
        {
            if (b == a) throw new ArgumentException("Cant create ray from two points that coincide!", nameof(b));
            return new Ray(a, (b - a));
        }

        public static Ray FromOriginAndDirection(Vec3 origin, Vec3 direction)
        {
            return new Ray(origin, direction);
        }
    }
}
