﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathTracer.Math
{
    public class Sphere
    {
        public Vec3 Centre { get; private set; } = new Vec3();
        public double Radius { get; private set; } = 1D;
        private readonly double radSq;

        public Sphere(Vec3 centre, double r)
        {
            Centre = centre;
            Radius = r;
            radSq = r * r;
        }

        public Hit Intersect(Ray ray)
        {
            var op = Centre - ray.Origin;
            var b = op.Dot(ray.Direction);
            var determinantSq = b * b - op.LengthSquared + radSq;
            if (determinantSq < 0) return null;

            var determinant = System.Math.Sqrt(determinantSq);
            const double epsilon = 1e-4;

            var minusT = b - determinant;
            var plusT = b + determinant;
            if (minusT < epsilon && plusT < epsilon) return null;

            var distanceToHit = minusT > epsilon ? minusT : plusT;
            var hitPosition = ray.PositionAlong(distanceToHit);
            var normal = (hitPosition - Centre).Normalised;
            if (normal.Dot(ray.Direction) > 0)
                normal = -normal;

            return new Hit
            {
                Distance = distanceToHit,
                Position = hitPosition,
                Normal = normal
            };
        }
    }
}
