﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathTracer.Math
{
    public class Vec3 : IEquatable<Vec3>
    {
        public Vec3()
        {
        }
        public Vec3(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double x { get; private set; } = 0;
        public double y { get; private set; } = 0;
        public double z { get; private set; } = 0;

        #region Axes
        public static Vec3 XAxis => new Vec3(1, 0, 0);
        public static Vec3 YAxis => new Vec3(0, 1, 0);
        public static Vec3 ZAxis => new Vec3(0, 0, 1);
        #endregion

        #region Operators
        public static Vec3 operator -(Vec3 input)
        {
            return new Vec3(-input.x, -input.y, -input.z);
        }

        public static Vec3 operator +(Vec3 a, Vec3 b)
        {
            return new Vec3(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static Vec3 operator -(Vec3 a, Vec3 b)
        {
            return a + -b;
        }

        public static Vec3 operator *(Vec3 a, double s)
        {
            return new Vec3(a.x * s, a.y * s, a.z * s);
        }

        public static Vec3 operator *(double s, Vec3 v)
        {
            return v * s;
        }

        public static Vec3 operator *(Vec3 a, Vec3 b)
        {
            return new Vec3(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static bool operator ==(Vec3 a, Vec3 b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Vec3 a, Vec3 b)
        {
            return !(a == b);
        }
        #endregion

        #region Vector operations and properties
        public double Dot(Vec3 other)
        {
            return x * other.x +
                   y * other.y +
                   z * other.z;
        }

        public double LengthSquared => Dot(this);

        public double Length => System.Math.Sqrt(LengthSquared);

        public Vec3 Normalised => this * (1.0 / Length);

        public void Normalise()
        {
            Vec3 v = Normalised;
            x = v.x;
            y = v.y;
            z = v.z;
        }

        public Vec3 Cross(Vec3 b)
        {
            var x = this.y * b.z - this.z * b.y;
            var y = this.z * b.x - this.x * b.z;
            var z = this.x * b.y - this.y * b.x;
            return new Vec3(x, y, z);
        }
        #endregion

        public override string ToString()
        {
            return $"Vec3({this.x},{this.y},{this.z})";
        }

        #region IEquatable
        public override bool Equals(object obj)
        {
            return Equals(obj as Vec3);
        }

        public bool Equals(Vec3 other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return x == other.x &&
                   y == other.y &&
                   z == other.z;
        }

        public override int GetHashCode()
        {
            var hashCode = 1753160635;
            hashCode = hashCode * -1521134295 + x.GetHashCode();
            hashCode = hashCode * -1521134295 + y.GetHashCode();
            hashCode = hashCode * -1521134295 + z.GetHashCode();
            return hashCode;
        }
        #endregion
    }
}
