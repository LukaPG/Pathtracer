﻿using PathTracer.Math;

namespace PathTracer.Objects
{
    public interface IPrimitive
    {
        IntersectionRecord Intersect(Ray ray);
    }

    public class IntersectionRecord
    {
        public Hit Hit { get; set; }
        public Material Material { get; set; }
    }
}
