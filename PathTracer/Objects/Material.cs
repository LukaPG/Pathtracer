﻿using PathTracer.Math;

namespace PathTracer.Objects
{
    public class Material
    {
        
        public Vec3 Emission { get; set; }
        public Vec3 Diffuse { get; set; }
        public static Material MakeDiffuse(Vec3 colour)
        {
            return new Material
            {
                Emission = new Vec3(),
                Diffuse = colour
            };
        }
    }
}
