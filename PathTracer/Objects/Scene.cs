﻿using System.Collections.Generic;
using PathTracer.Math;

namespace PathTracer.Objects
{
    public class Scene : IPrimitive
    {
        public List<IPrimitive> Primitives { get; set; }
        public IntersectionRecord Intersect(Ray ray)
        {
            IntersectionRecord currentNearest = null;
            foreach (var primitive in Primitives)
            {
                var intersection = primitive.Intersect(ray);
                if (intersection == null)
                    continue;
                if (currentNearest == null || intersection.Hit.Distance < currentNearest.Hit.Distance)
                {
                    currentNearest = intersection;
                }
            }
            return currentNearest;
        }

        public void Add(IPrimitive primitive)
        {
            Primitives.Add(primitive);
        }
    }
}
