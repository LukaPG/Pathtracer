﻿using NUnit.Framework;
using PathTracer.Math;

namespace PathTracer.Tests
{
    [TestFixture]
    public class Vec3Tests
    {

        [Test]
        public void IsDefaultConstructable()
        {
            Vec3 vector = new Vec3();
            Assert.IsNotNull(vector);
            Assert.AreEqual(0, vector.x);
            Assert.AreEqual(0, vector.y);
            Assert.AreEqual(0, vector.z);
            Assert.AreEqual(0, vector.LengthSquared);
            Assert.AreEqual(0, vector.Length);
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(1, 2, 3)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(-1, -1, -1)]
        public void IsConstructableWithCoordinates(double x, double y, double z)
        {
            Vec3 vector = new Vec3(x, y, z);
            Assert.IsNotNull(vector);
            Assert.AreEqual(x, vector.x);
            Assert.AreEqual(y, vector.y);
            Assert.AreEqual(z, vector.z);
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(1, 2, 3)]
        [TestCase(-1, -1, -1)]
        public void DotProductMath(double x, double y, double z)
        {
            Vec3 v1 = new Vec3(x, y, z);
            Vec3 v2 = new Vec3(2, 3, 4);

            var dotProduct = v1.Dot(v2);

            var expected = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;

            Assert.AreEqual(expected, dotProduct);
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(1, 2, 3)]
        [TestCase(-1, -1, -1)]
        public void CrossProductMath(double x, double y, double z)
        {
            Vec3 v1 = new Vec3(x, y, z);
            Vec3 v2 = new Vec3(1.23, 3.48, -4.52);

            var crossProduct = v1.Cross(v2);

            var _x = v1.y * v2.z - v1.z * v2.y;
            var _y = v1.z * v2.x - v1.x * v2.z;
            var _z = v1.x * v2.y - v1.y * v2.x;
            var expected = new Vec3(_x, _y, _z);

            Assert.AreEqual(expected, crossProduct);
        }

        [Test]
        public void Normalised()
        {
            Vec3 v = new Vec3(432, 54.1, -14.43);

            var normal = v.Normalised;

            Assert.AreEqual(1, normal.Length, 0.000001);
        }

        [Test]
        public void Normalise()
        {
            Vec3 v = new Vec3(432, 54.1, -14.43);

            v.Normalise();

            Assert.AreEqual(1, v.Length, 0.000001);
        }

        [Test]
        [TestCase(1, 0, 0)]
        [TestCase(0, 1, 0)]
        [TestCase(0, 0, 1)]
        public void LenghtOfOne(double x, double y, double z)
        {
            var v = new Vec3(x, y, z);

            Assert.AreEqual(1, v.Length);
            Assert.AreEqual(1, v.LengthSquared);
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(1, 2, 3)]
        [TestCase(-1, -1, -1)]
        public void Lenght(double x, double y, double z)
        {
            var v = new Vec3(x, y, z);

            var expected = v.Dot(v);

            Assert.AreEqual(expected, v.LengthSquared);
            Assert.AreEqual(System.Math.Sqrt(expected), v.Length);
        }

        #region operators
        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(1, 2, 3)]
        [TestCase(-1, -1, -1)]
        public void Equality(double x, double y, double z)
        {
            var v1 = new Vec3(x, y, z);
            var v2 = new Vec3(x, y, z);
            var v3 = new Vec3();

            // Equality
            Assert.IsTrue(v1 == v2, "eq1");
            Assert.IsTrue(v2 == v1, "eq2");

#pragma warning disable CS1718 // Comparison made to self
            Assert.IsTrue(v1 == v1, "self1");
            Assert.IsTrue(v2 == v2, "self2");
#pragma warning restore CS1718 // Comparison made to self

            // Inequality
            Assert.IsTrue(v1 != v3, "ne1");
            Assert.IsTrue(v2 != v3, "ne2");
            Assert.IsTrue(v3 != v2, "ne3");
            Assert.IsTrue(v3 != v1, "ne4");
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(1, 2, 3)]
        [TestCase(-1, -1, -1)]
        public void Add(double x, double y, double z)
        {
            var v1 = new Vec3(x, y, z);
            var v2 = new Vec3(1, 2, 3);
            var exp = new Vec3(x + 1, y + 2, z + 3);

            var result = v1 + v2;
            var commutes = result == v2 + v1;

            Assert.AreEqual(exp, result);
            Assert.IsTrue(commutes);
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(1, 2, 3)]
        [TestCase(-1, -1, -1)]
        public void Sub(double x, double y, double z)
        {
            var v1 = new Vec3(x, y, z);
            var v2 = new Vec3(1, 2, 3);
            var exp = new Vec3(x - 1, y - 2, z - 3);

            var result = v1 - v2;

            Assert.AreEqual(exp, result);
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(1, 2, 3)]
        [TestCase(-1, -1, -1)]
        public void Mul(double x, double y, double z)
        {
            var v1 = new Vec3(x, y, z);
            var scalar = 42;
            var exp = new Vec3(x *scalar, y * scalar, z * scalar);

            var result = v1 * scalar;
            var commutes = result == scalar * v1;

            Assert.AreEqual(exp, result);
            Assert.IsTrue(commutes);
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(-2.1, 43, 943.432)]
        [TestCase(1, 2, 3)]
        [TestCase(-1, -1, -1)]
        public void Negate(double x, double y, double z)
        {
            var v1 = new Vec3(x, y, z);
            var exp = new Vec3(-x, -y, -z);

            var result = -v1;

            Assert.AreEqual(exp, result);
        }
        #endregion
    }
}
